FROM docker.io/wordpress:latest
USER root

MAINTAINER Paolo Panigada

ENV WORDPRESS_DB_USER user
ENV WORDPRESS_DB_PASSWORD password
ENV WORDPRESS_DB_NAME db
ENV WORDPRESS_DB_HOST mysql.develop.svc.cluster.local

ADD app/2irg.tar /var/www/html/wp-content/themes/
RUN chown -R www-data:www-data /var/www/html/wp-content/themes/
    
EXPOSE 80